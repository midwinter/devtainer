FROM alpine:3.14

RUN apk add -U --no-cache nodejs-current npm \
    neovim git git-perl \
    fish tmux openssh-client openssh-server bash ncurses \
    curl less python3 python3-dev py3-pip ca-certificates tzdata \
    neovim-doc gcc libc-dev musl-dev build-base && \
    rm -rf /tmp/* /etc/apk/cache/*
RUN npm install -g n
RUN pip3 install neovim
RUN ln -s /usr/bin/nvim /usr/bin/vim
COPY --from=golang:1.14-alpine /usr/local/go/ /usr/local/go/
 
ENV PATH="/usr/local/go/bin:${PATH}"

# Create a user called 'me'
RUN adduser --disabled-password  me
# Do everything from now in that users home directory
USER me
WORKDIR /home/me
ENV HOME /home/me
ENV N_PREFIX /home/me/.n
RUN mkdir -p $N_PREFIX
RUN mkdir -p /home/me/.config/nvim
RUN mkdir -p /home/me/.config/cocVim
RUN n lts
COPY vimrc /home/me/.config/nvim/init.vim
COPY tmux.conf /home/me/.tmux.conf
RUN curl -fLo /home/me/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
#RUN nvim --headless +GoInstallBinaries +qall
RUN nvim --headless +PlugInstall +qall
#This timeout is not ideal, but the install seems to exit immediately without it
RUN timeout 5m nvim --headless +CocInstall; exit 0

ENV SHELL /usr/bin/fish
WORKDIR /home/me/workspace 

CMD ["fish"]
