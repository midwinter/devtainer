#!/bin/bash

IMAGE_NAME=registry.gitlab.com/midwinter/devtainer:latest 

mnt=$(pwd)
while getopts ":v:u:" o; do
    case "${o}" in
        v)
            mnt=${OPTARG}
            ;;
        u)
            docker pull ${IMAGE_NAME} 
            ;;
    esac
done
shift $((OPTIND-1))
docker run -it -v ${mnt}:/home/me/workspace ${IMAGE_NAME} $@ 
