# Snowglobe Devtainer

My portable dev environment with nvim and tmux. Just requires Docker! This is a work in progress

## Installation

1. Download the script to somewhere in your $PATH (in my case I add ~/.local/bin to my path) and add execution permissions

```
wget https://gitlab.com/midwinter/devtainer/-/blob/main/snowglobe.sh -O ~/.local/bin/sg
chmod +x ~/.local/bin/sg
```

2. Launch vim or something!
```
sg vim
```

