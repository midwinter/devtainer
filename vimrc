if empty(glob('~/.config/nvim/autoload/pathogen.vim'))
    silent !curl -LSso ~/.config/nvim/autoload/pathogen.vim --create-dirs
                            \ https://tpo.pe/pathogen.vim 
endif
if empty(glob('~/.config/nvim/autoload/plug.vim'))
    silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
                            \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

if &shell =~# 'fish$'
    set shell=sh
endif

call plug#begin('~/.config/nvim/plugged')
    Plug 'leafgarland/typescript-vim'
    Plug 'peitalin/vim-jsx-typescript'
    Plug 'ryanoasis/vim-devicons'
    Plug 'scrooloose/nerdcommenter'
    Plug 'pangloss/vim-javascript'
    Plug 'mxw/vim-jsx'
    Plug 'rking/ag.vim'
    Plug 'ap/vim-css-color'
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'kien/ctrlp.vim'
    Plug 'dylanaraps/wal.vim'
    Plug 'https://gitlab.com/rj-white/vim-colors-paramountblue'
    Plug 'airblade/vim-gitgutter'
    Plug 'editorconfig/editorconfig-vim'
    Plug 'tpope/vim-fugitive'
    Plug 'tpope/vim-surround'
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'SirVer/ultisnips'
    Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
    Plug '907th/vim-auto-save'
    Plug 'jiangmiao/auto-pairs'
    Plug 'OmniSharp/omnisharp-vim'
    Plug 'hashivim/vim-terraform'
    Plug 'ruanyl/coverage.vim'
call plug#end()

execute pathogen#infect()

let mapleader="\\"

noremap <Leader>y "*y
noremap <Leader>p "*p
noremap <Leader>Y "+y
noremap <Leader>P "+p

noremap <Leader>g :Gblame<CR>

map <C-n> :e.<CR>

" Make J and K nav go by visual line
nnoremap <expr> j v:count ? (v:count > 5 ? "m'" . v:count : '') . 'j' : 'gj'
nnoremap <expr> k v:count ? (v:count > 5 ? "m'" . v:count : '') . 'k' : 'gk'

set hidden
set hlsearch	"" Highlight all search results
set smartcase	"" Enable smart-case search
set ignorecase	"" Always case-insensitive
set incsearch	"" Searches for strings incrementally
set softtabstop=4 shiftwidth=4 tabstop=4 expandtab   "" use 4 spaces
set exrc "" Project specific vimrcs
set secure "" No unsafe commands

set t_Co=256
"set background=dark
":colorscheme paramountblue
:colorscheme wal


" automatically leave insert mode after 'updatetime' milliseconds of inaction
set updatetime=5000
au CursorHoldI * stopinsert


"""
""" Create dir if doesn't exist on save
"""
augroup vimrc-auto-mkdir
	autocmd!
	autocmd BufWritePre * call s:auto_mkdir(expand('<afile>:p:h'), v:cmdbang)
	function! s:auto_mkdir(dir, force)
		if !isdirectory(a:dir)
					\   && (a:force
					\       || input("'" . a:dir . "' does not exist. Create? [y/N]") =~? '^y\%[es]$')
			call mkdir(iconv(a:dir, &encoding, &termencoding), 'p')
		endif
	endfunction
augroup END

"""
""" line numbers
"""
:set number relativenumber

:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

"""
""" netrw
"""
let g:netrw_banner = 0

"""
""" Fish
"""
syntax enable
filetype plugin indent on
" Set up :make to use fish for syntax checking.
autocmd FileType fish compiler fish

" Set this to have long lines wrap inside comments.
autocmd FileType fish setlocal textwidth=79

" Enable folding of block structures in fish.
autocmd FileType fish setlocal foldmethod=expr

"""
""" COC completion
"""
"let g:coc_node_path ='/home/craig/.nvm/versions/node/v10.16.3/bin/node'
autocmd FileType json syntax match Comment +\/\/.\+$+
let g:coc_global_extensions = [
  \ 'coc-css',
  \ 'coc-emmet',
  \ 'coc-highlight',
  \ 'coc-html',
  \ 'coc-json',
  \ 'coc-snippets',
  \ 'coc-graphql',
  \ 'coc-eslint',
  \ 'coc-prettier',
  \ 'coc-fish',
  \ 'coc-tsserver',
  \ 'coc-styled-components',
  \ 'coc-python',
  \ 'coc-sh',
  \ 'coc-rls'
\ ]

inoremap <silent><expr> <c-space> coc#refresh()
" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

"""
""" Ultisnips
"""
"let g:UltiSnipsSnippetsDir="~/.config/nvim/snips"
let g:UltiSnipsSnippetDirectories=["snips", "Ultisnips"]

"""
""" Airline
"""
let g:airline_theme='lucius'
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#syntastic#enabled = 1
set laststatus=2

"""
""" Ale Linting
"""
"let g:ale_sign_error = ''
"let g:ale_sign_warning = '⚠️'
"let g:ale_lint_on_enter = 0 "
"let g:ale_fixers = {'javascript':['eslint'],'scss':['prettier'] }
let g:ale_linters = {'cs':['OmniSharp'], 'typescript':['eslint', 'tsserver']}
let g:ale_fixers = {'javascript':['prettier','eslint'],'typescript':['prettier','eslint'],'scss':['prettier'], 'json':['prettier'], 'yaml':['prettier']}
let g:ale_fix_on_save = 1
let g:ale_lint_on_text_changed = 'normal'
let g:ale_lint_on_insert_leave = 1
let g:ale_set_highlights = 1


"""
""" TerraformVim
"""
let g:terraform_align=1
let g:terraform_fold_sections=1
let g:terraform_fmt_on_save=1

"""
""" CtrlP
"""
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git\|dist\|build'
nmap <Leader>b :CtrlPBuffer<CR>

"""
""" OmniSharp
"""
let g:OmniSharp_server_stdio = 1
let g:OmniSharp_timeout = 5
let g:OmniSharp_selector_ui = 'ctrlp'  " Use ctrlp.vim
set completeopt=longest,menuone,preview
nnoremap <Leader>fc :OmniSharpCodeFormat<CR>

autocmd FileType cs autocmd BufWritePre <buffer> :OmniSharpCodeFormat


"""
""" AG search
"""
let g:ag_prg="/usr/bin/ag --vimgrep --ignore={'*.har'}"


"""
""" Keys for Buffer nav
"""
:nnoremap <Tab> :bnext<CR>:checktime<CR>
:nnoremap <S-Tab> :bprevious<CR>:checktime<CR>
:nnoremap <C-X> :bdelete<CR>

filetype plugin on


"""
""" File formats
"""
"""Fountain"""
au BufRead,BufNewFile *.fountain	set filetype=fountain
"""YAML"""
au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml foldmethod=indent
"au BufNewFile,BufRead *.{ts,tsx} set filetype=typescript
"au BufNewFile,BufRead *.{js,jsx} set filetype=javascript
"au BufNewFile,BufRead *.{cs} set filetype=csharp
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

"""Coverage"""
" Specify the path to `coverage.json` file relative to your current working directory.
let g:coverage_json_report_path = 'src/__tests__/coverage/coverage-final.json'

let g:coverage_enabled = 1

" Define the symbol display for covered lines
let g:coverage_sign_covered = '⦿'
let g:coverage_sign_uncovered = 'X'

" Define the interval time of updating the coverage lines
let g:coverage_interval = 5000

" Do not display signs on covered lines
let g:coverage_show_covered = 1

" Display signs on uncovered lines
let g:coverage_show_uncovered = 1

au FileType typescript map <C-f> :CocCommand eslint.executeAutofix<CR>
au FileType javascript.jsx map <C-f> :CocCommand eslint.executeAutofix<CR>
au FileType javascript map <C-f> :CocCommand eslint.executeAutofix<CR>
au FileType cs map <C-f> :OmniSharpCodeFormat<CR>
